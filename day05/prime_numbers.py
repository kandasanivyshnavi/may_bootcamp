import math
def prime_generator(limit: int) -> list[int]:
    prime_nums = [2, 3]
    if limit == 3:
        return [2]
    elif limit == 4:
        return [2,3]
    num = 1
    prime = 0
    while(prime < limit):
        prime = 6 * num - 1
        prime_nums.append(prime)
        prime = 6 * num + 1
        prime_nums.append(prime)
        num += 1
    return prime_nums
print(prime_generator(10))