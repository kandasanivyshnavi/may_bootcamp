# n=10
# print(type(n))
#output: <class 'int'>
'''class items:
    def average(self,x,y):
        return x*y
marks = items()
marks.student = "student1"
marks.m1 = 5
marks.m2 = 6
print(marks.average(marks.m1 , marks.m2))

marks2 = items()
marks2.student = "student1"
marks2.m1 = 5
marks2.m2 = 6
print(marks2.average(marks2.m1 , marks2.m2))'''

#output: 30
#....................................................#
'''class items:
    def __init__(self,student) -> None:
        print(student)

marks = items("student1")
marks.student = "student1"
marks.m1 = 5
marks.m2 = 6

marks2 = items("student2")
marks2.m1 = 5
marks2.m2 = 4'''

# outputs :
#           student1
#           student2
# here we are using atributes
#................................................#
'''class items:
    def __init__(self,student,m1,m2) -> None:
        self.m1 = m1
        self.m2 =m2
        self.student =student          
        print(student,m1*m2)

marks = items("vyshnavi",4,3)

marks2 = items("vyshu",5,6)


print(marks2.m2) # output : 6
print(marks.student) '''# output : vyshnavi

#output: vyshnavi 12
#         vyshu 30
# __init__ function used to print  something
# even we dont need to write the atributes 
# we just need object and a method
#.........................................#






