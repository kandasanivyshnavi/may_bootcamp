files = ['day1.txt', 'day10.txt', 'day11.txt', 'day12.txt', 'day13.txt', 'day14.txt', 'day2.txt', 'day3.txt', 'day4.txt', 'day5.txt', 'day6.txt', 'day7.txt', 'day8.txt', 'day9.txt']

def clean(filenames: list[str], prefix: str, extension: str) -> list[str]:
    return [int(file.replace(prefix, "").replace(extension,"")) for file in filenames]

def sorted_files(filenames: list[int],prefix: str,extension: str):
    return [prefix + str(file_num) + extension for file_num in sorted(clean(filenames, prefix, extension))]
print(sorted_files(files,"day",".txt"))
#this is not a correct code an dnot that much efficient