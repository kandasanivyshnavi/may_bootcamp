def jail_cells (limit: int) -> list[int]: 
    doors = [True for _ in range (limit + 2)] 

    for door in range(1, limit + 1): 
        for _ in range(door, limit+1, door): 
            doors[_] = not doors[_]

    return [i for i in range(1, limit + 2) if doors[i] == False]

print(jail_cells(100))