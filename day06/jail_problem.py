def open_doors(no_of_doors: int,turns: int):
    open_door = [n for n in range(1, no_of_doors + 1)]
    close_door = []
    for turn in range(1, turns + 1):
        for door in range(turn, no_of_doors + 1, turn):
            if  door in open_door:
                open_door.remove(door)
                close_door.append(door)
            elif door in close_door:
                close_door.remove(door)
                open_door.append(door)
    return close_door
print(open_doors(10,2))