def is_ascending(s: str) -> bool:
    return s == ''.join(sorted(set(s)))

def is_descending(s: str) -> bool:
    return s[::-1] == ''.join(sorted(set(s)))

def is_peak(s: str) -> bool:
    first,second = s.split(max(s),maxsplit=1)
    return is_ascending(first) and is_descending(second)

def is_valley(s: str) -> bool:
    first,second = s.split(max(s),maxsplit=1)
    return is_descending(first) and is_ascending(second)

def classify(s: str) -> str:
    if is_ascending(s):
        return 'A'
    if is_descending(s):
        return 'D'
    if is_peak(s):
        return 'P'
    if is_valley(s):
        return 'V'
    
print(classify('12921'))