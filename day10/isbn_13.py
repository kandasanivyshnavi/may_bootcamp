# ls
# num=978059652068
# res = [int(x) for x in str(num)]
# # print(res)
# evens=[]
# sum = 0
# for i in range(len(res)):
#     if i % 2 == 0:
#         evens.append(res[i] )
#     else:
#         evens .append(res[i] * 3)    

# print(evens)
# for i in evens:
#     sum += i

# reminder = sum % 10

# result = 10 - reminder
# if result == 10:
#     print('0')
# else :
#     print(result)    
# print(result)
# print(reminder)    
# print(sum)

# def sum_(num) ->int:
#     res = [int(x) for x in str(num)]
#     evens=[]
#     sum_is = 0
#     for i in range(len(res)):
#         if i % 2 == 0:
#             evens.append(res[i] )
#     else:
#         evens .append(res[i] * 3)    

#     for i in evens:
#         sum_is += i
#     return sum_is

# def reminder_(num :int) -> int:

#     rem= sum_(num) % 10
#     return rem

# def result(num) ->int:
#     result = 10 - reminder_(num)
#     if result == 10:
#         return ('0')
#     else :
#        return (result)
    
# num=978059652068
# print(result(num))
def sum_(num) -> int:

    res = [int(x) for x in str(num)]
    evens = []
    sum_is = 0
 
    for i in range(len(res)):
        if i % 2 == 0:
            evens.append(res[i])
        else:
            evens.append(res[i] * 3)  
    
    for i in evens:
        sum_is += i
    return sum_is

def reminder_(num: int) -> int:
    rem = sum_(num) % 10
    return rem

def result(num) -> int:
    
    res = 10 - reminder_(num)
    if res == 10:
        return 0  
    else:
        return res


num = 978059652068
print(result(num))
    