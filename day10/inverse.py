def inverse(a, n):
    t, newt = 0, 1
    r, newr = n, a

    while newr != 0:
        quotient = r // newr
        t, newt = newt, t - quotient * newt
        r, newr = newr, r - quotient * newr

    if r > 1:
        return "a is not invertible"
    if t < 0:
        t = t + n

    return t

# Example usage:
a = 5133
n = 7
result = inverse(a,n)
print(f"The modular inverse of {a} modulo {n} is {result}")