def frequency_calculator(word: str) -> dict:
    frequency = {}
    for letter in word:
        if letter not in frequency.keys():
            frequency.update({letter : 1})
        else:
            frequency[letter] += 1
    
    for key in frequency.keys():
        frequency[key] = round((frequency[key] * 100)/ len(word),2)
    return frequency
print(frequency_calculator("@@@@@@"))
print(frequency_calculator("hello"))

# def load_data(filename: str) -> str:
#     with open(filename) as file:
#         content = file.read()
#     return content