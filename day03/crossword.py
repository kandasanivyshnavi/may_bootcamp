"""[
wwwwwwbwwwwwwww
wbwbwbwbwbwbwbw
wwwwwwwwwbwwwww
wbwbwbwbwbwbwbw
wwwwwbwwwwwwwww
wbwbbbwbwbwbwbw
wwwwwwwbwwwwbbb
wbwbwbbbbbwbwbw
bbbwwwwbwwwwwww
wbwbwbwbwbbbwbw
wwwwwwwwwbwwwww
wbwbwbwbwbwbwbw
wwwwwbwwwwwwwww
wbwbwbwbwbwbwbw
wwwwwwwwbwwwwww
]"""

"""
1,7
2,22,42,62,82,102,122,14
3,10
4,24,44,64,84,104,124,14
5,6
6,26,46,56,66,86,106,126,14
7,87,137,147,15
8,28,48,68,78,88,98,108,128,14
9,19,29,39,8
10,210,410,610,810,1010,1110,1210,14
11,10
12,212,412,612,812,1012,1212,14
13,6
14,214,414,614,814,1014,1214,14
15,9"""


import pprint 
# import sys

LF, BLACK, WHITE = "\n", "#", " "

gridfile = sys.argv[1]
# DEBUG = len(sys.argv) = 3

# LOAD THE GRID

grid = []
for line in open(sys.argv[1]):
    grid.append(line.strip(LF))

if DEBUG:
    pprint.pprint(grid)

# ADD SENTINEL

size = len(grid)
grid.insert(0, BLACK * size)
grid.append(BLACK * size)

grid = [BLACK + line+ BLACK for line in grid]
if DEBUG:
    pprint.pprint(grid)

# TRANSPOSE GRID
transpose = []
for col in range(len(grid)):
    s=""
    for row in grid:
        s+=row[col]
    transpose.append(s)
if DEBUG:
    pprint.pprint(transpose)

# COMBINE INTO LINES

across = "".join(grid)